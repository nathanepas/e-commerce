<?php
session_start();
include 'connect.php';

$email = stripslashes($_POST['email']);
$password = stripslashes($_POST['password']);

if(mysqli_connect_errno()){
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit();
}

$stmt = $conn->prepare("SELECT password_hash FROM users WHERE email = ?");
$stmt->bind_param("s", $email);
$stmt->execute();

$result = $stmt->get_result();
$resultrows = mysqli_num_rows($result);

switch ($resultrows){
	case 0 :
		echo "No results found";
		break;
	case 1 :
		break;
	default :
		echo $resultrows . " rows found. Major error";
}

if($resultrows == 1){
	
	$row = $result->fetch_assoc();
	$hash = $row['password_hash'];
	
	$verify = password_verify($password, $hash);
	$stmt2 = $conn->prepare("SELECT * FROM users WHERE email = ?");
	$stmt2->bind_param("s", $email);
	$stmt2->execute();
	$result2 = $stmt2->get_result();
	$resultrows2 = mysqli_num_rows($result2);
	if($resultrows2 == 1 && $verify == 1){
		
		$row2 = $result2->fetch_assoc();
		
		if($row2['email_verify'] == 1){
		$_SESSION['email'] = $row2['email'];
		$_SESSION['name'] = $row2['name'];
		$_SESSION['business'] = $row2['business'];
		$_SESSION['phone'] = $row2['phone'];
		$_SESSION['billing_addr1'] = $row2['b_addr1'];
		$_SESSION['billing_addr2'] = $row2['b_addr2'];
		$_SESSION['billing_pc'] = $row2['b_pc'];
		$_SESSION['billing_city'] = $row2['b_city'];
		$_SESSION['delivery_addr1'] = $row2['d_addr1'];
		$_SESSION['delivery_addr2'] = $row2['d_addr2'];
		$_SESSION['delivery_pc'] = $row2['d_pc'];
		$_SESSION['delivery_city'] = $row2['d_city'];
		$_SESSION['logged_in'] = 1;
		$_SESSION['user_id'] = $row2['id'];
		$_SESSION['state'] = $row2['state'];

		header("location: ../../index.php");
		}else{
			echo "Please verify your email";
		}
	}else{
		echo "Password incorrect";
		exit();
	}
	
	
	
}else{
	exit();
}
?>