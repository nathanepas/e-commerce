<?php
	session_start();
	include 'src/php/connect.php';

	if(isset($_SESSION['logged_in'])){
		header("location: index.php");
	}
?>
<html>

<head>
	<link rel='stylesheet' href='src/css/index.css' type='text/css'>
	<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
	<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

</head>

<body>
	<nav>
		<div class="nav-wrapper">
			<div class='container'>
				<a href="/" class="brand-logo sidenav-trigger img_container"><img class='img_logo' src='src/img/gs_logo.png'></a>
				<ul id="mobile-demo" class="right hide-on-med-and-down">
					<li><a href="/about.php">About</a></li>
					<li><a href="/contact.php">Contact</a></li>
					<li><a href="/test.php">Cart <i class="fas fa-shopping-cart fa_src"></i></a></li>
					<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account <i class="fas fa-user fa_src"></i>

	</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<ul id="dropdown1" class="dropdown-content">
		<?php
	 if(isset($_SESSION['logged_in'])){ if($_SESSION['logged_in'] == 1){ echo "
	<li><a href='profile.php'>Profile</a></li>"; echo "
	<li><a href='orders.php'>Orders</a></li>"; echo "
	<li><a href='settings.php'>Settings</a></li>"; echo "
	<li class='divider'></li>"; if($_SESSION['state'] == 1){ echo "
	<li><a href='admin.php'>All Orders</a></li>"; echo "
	<li><a href='order_items.php'>All Products</a></li>"; echo "
	<li class='divider'></li>"; } echo "
	<li><a href='src/php/logout.php'>Logout</a></li>"; } }else{ echo "
	<li><a href='login.php'>Login</a></li>"; echo "
	<li><a href='register.php'>Register</a>
		<li>"; }
	?>
	</ul>
	<div class='container'>
		<div class='row'>
			<form method='post' action='src/php/dolog.php'>
				<div class='col s12 m8' id='login_form'>
					<div class='input-field'>
						<label for='email'>Email</label>
						<input type='email' id='email' name='email'>
					</div>
					<div class='input-field'>
						<label for='password'>Password</label>
						<input type='password' id='password' name='password'>
					</div>

					<input type='submit' class='btn' placeholder='LOGIN'>
				</div>
			</form>
		</div>
	</div>
</body>

</html>
