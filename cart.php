<?php
	echo "hello";
?>
	<html>

	<head>

		<script src="https://cdn.worldpay.com/v1/worldpay.js"></script>
		<script type='text/javascript'>
			window.onload = function() {
				Worldpay.useTemplateForm({
					'clientKey': 'T_C_85bbb*c66-7390-4039-84e4-13809a7a3b70',
					'form': 'paymentForm',
					'paymentSection': 'paymentSection',
					'display': 'inline',
					'reusable': true,
					'callback': function(obj) {
						if (obj && obj.token) {
							var _el = document.createElement('input');
							_el.value = obj.token;
							_el.type = 'hidden';
							_el.name = 'token';
							document.getElementById('paymentForm').appendChild(_el);
							document.getElementById('paymentForm').submit();
						}
					}
				});
			}

		</script>
	</head>

	<body>
		<div id='ok'></div>

		<script>
			var sumtotal = 0;

			for (var i = 0; i < sessionStorage['array'].length; i += 2) {

				var dataObj = JSON.parse(sessionStorage['data'])[sessionStorage['array'][i]];
				var title = dataObj.name;
				var id = dataObj.id;
				var cost = dataObj.cost_pu;
				var quant = dataObj.quantity;

				var total = parseFloat(quant) * parseFloat(cost);
				sumtotal += total;

				var para = document.createElement("p");
				var content = document.createTextNode(title + ", Quantity: " + quant + " x £" + cost + " = £" + total);

				para.appendChild(content);

				var currentdiv = document.getElementById("ok");

				document.body.insertBefore(para, currentdiv);


			}

			console.log("Sumtotal = £" + sumtotal);

		</script>

		<form action="/complete" id="paymentForm" method="post">
			<!-- all other fields you want to collect, e.g. name and shipping address -->
			<div id='paymentSection'></div>
			<div>
				<input type="submit" value="Place Order" onclick="Worldpay.submitTemplateForm()" />
			</div>
		</form>
		<button onclick='pay()'>Pay</button>
	</body>

	</html>
