<?php
	session_start();
?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>GreaseShield Product Range!</title>
	<link rel='stylesheet' href='src/css/index.css' type='text/css'>
	<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


    <?php	include 'src/php/connect.php';	?>
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body>
	<nav>
		<div class="nav-wrapper">
			<div class='container'>
				<a href="/" class="brand-logo sidenav-trigger img_container"><img class='img_logo' src='src/img/gs_logo.png'></a>
				<ul id="mobile-demo" class="right hide-on-med-and-down">
					<li><a href="/about.php">About</a></li>
					<li><a href="/contact.php">Contact</a></li>
					<li><a href="/test.php">Cart <i class="fas fa-shopping-cart fa_src"></i></a></li>
					<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account <i class="fas fa-user fa_src"></i>

	</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<ul id="dropdown1" class="dropdown-content">
		<?php
	 if(isset($_SESSION['logged_in'])){ if($_SESSION['logged_in'] == 1){ echo "
	<li><a href='profile.php'>Profile</a></li>"; echo "
	<li><a href='orders.php'>Orders</a></li>"; echo "
	<li><a href='settings.php'>Settings</a></li>"; echo "
	<li class='divider'></li>"; if($_SESSION['state'] == 1){ echo "
	<li><a href='admin.php'>All Orders</a></li>"; echo "
	<li><a href='order_items.php'>All Products</a></li>"; echo "
	<li class='divider'></li>"; } echo "
	<li><a href='src/php/logout.php'>Logout</a></li>"; } }else{ echo "
	<li><a href='login.php'>Login</a></li>"; echo "
	<li><a href='register.php'>Register</a>
		<li>"; }
	?>
	</ul>
<div class='container'>
    <h4>Contact Form</h4>
<div class='row'>
    <div class='col m12 l6'>
        <form method='post'>
            <div class='input-field'>
                <label for='name'>Name</label>
                <input type='text' id='name' name='name'>
            </div>
            <div class='input-field'>
                <label for='email'>Email</label>
                <input type='email' id='email' name='email'>
            </div>
            <div class="input-field">
                  <textarea id="textarea1" rows="100" class="materialize-textarea" name='message'></textarea>
                  <label for="textarea1">Information</label>
            </div>
            <div class="g-recaptcha" data-sitekey="6LeRAVMUAAAAAAlJx91GUsOOdmxeQm0vmZdvhT0N"></div><br />
            <input type='submit' class='btn'>
        </form>
    </div>
    <div class='col m12 l6'>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2333.717251002201!2d-6.353365783782051!3d54.202790330167375!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4860dcca9c64ff9b%3A0xd024ba4fe147d40!2sEnvironmental+Products+and+Services+Ltd!5e0!3m2!1sen!2suk!4v1523613901344"  height="450px" frameborder="0" style="position:relative; width: 100%; border:0"></iframe>

    </div>
    </div>
</div>
<footer class="page-footer">

    <div class="footer-copyright">
        <div class="container">
            © 2018 Environmental Products &amp; Services Ltd.

        </div>
    </div>
</footer>
<?php
	function post_captcha($user_response) {
		$fields_string = '';
		$fields = array(
			'secret' => '6LeRAVMUAAAAAOHuoPF6GvoLp2J5LQpiSoNlcNYR',
			'response' => $user_response
		);
		foreach($fields as $key=>$value)
		$fields_string .= $key . '=' . $value . '&';
		$fields_string = rtrim($fields_string, '&');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

		$result = curl_exec($ch);
		curl_close($ch);

		return json_decode($result, true);
	}

	// Call the function post_captcha
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
	$res = post_captcha($_POST['g-recaptcha-response']);

	if (!$res['success']) {
		// What happens when the CAPTCHA wasn't checked
		echo '<p>Please go back and make sure you check the security CAPTCHA box.</p><br>';
	} else {
		// If CAPTCHA is successfully completed...
		$to = "enquiries@greaseshield.co.uk";
		$subject = "enquiry";
		$message = "from: " . $_POST['name'] . ", " . $_POST['email'] . "<br /><br />" . $_POST['message'];
		$headers = array(
		"From" => "webmaster@greaseshield.co.uk"
		);
		mail($to, $subject, $message, $headers);
		// Paste mail function or whatever else you want to happen here!
		echo '<br><p>CAPTCHA was completed successfully!</p><br>';
	}
}
?>
</body>
</html>
