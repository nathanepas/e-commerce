function checkzero(a) {

    a = "" + a;
    if (a.indexOf(".") != -1) {
        var ok = a.substring(a.length - 2, a.length - 1);
        if (ok == ".") {
            return parseFloat(a) + "0";
        } else {
            return parseFloat(a);
        }
    } else {
        return parseFloat(a);
    }

}

function minBut(a) {
    var id = a.split("_")[0];
    var parser = JSON.parse(sessionStorage.getItem('data'));
    for (var i = 0; i < parser.length; i++) {
        if (parser[i].id == id) {
            if (parser[i].quantity > 1) {
                parser[i].quantity--;
                sessionStorage.setItem('data', JSON.stringify(parser));
            } else if (parser[i].quantity == 1) {
                parser.splice(i, 1);
                sessionStorage.setItem('data', JSON.stringify(parser));
                document.getElementById(id + '_cart').innerHTML = '';
            }
        }
    }
    newBasket();
}

function search() {

    var search_val = document.getElementById('search_box').value.toLowerCase();
    var classval = document.getElementsByClassName('product_title');
    var overall = document.getElementsByClassName('over_container');
    if (search_val) {

        for (var i = 0; i < classval.length; i++) {
            if (classval[i].innerHTML.toLowerCase().indexOf(search_val) != -1) {
                overall[i].style.display = 'block';
            } else {
                overall[i].style.display = 'none';
            }
        }

    }
    if (search_val == "") {
        for (var i = 0; i < classval.length; i++) {
            overall[i].style.display = 'block';
        }
    }
}


function show() {

    if (sessionStorage.getItem('data')) {

        var parser = JSON.parse(sessionStorage.getItem('data')); // [{id1, name1, cost1, quant1}, {id2, name2, cost2, quant2}]
        var sumtot = 0;
        for (var i = 0; i < parser.length; i++) {
            if (parser[i].quantity >= 1) {
                var uid = parser[i].id;
                uid = parseInt(uid);
                console.log(uid);
                var product = document.createElement("p");
                product.setAttribute("id", uid + "_cart");
                product.setAttribute("class", "cartClass");
                var seshparse = parser[i];

                var button = document.createElement("button");
                var txtNode = document.createTextNode('-');
                button.appendChild(txtNode);
                button.setAttribute("id", uid + "_minbut");
                button.addEventListener("click", function() {
                    minBut(this.id);
                    displayTotal();
                });
                button.setAttribute("class", "sml");
                product.appendChild(button);
                var content = document.createTextNode(seshparse.name + " : " + seshparse.quantity + " x £" + checkzero(seshparse.cost_pu));
                product.appendChild(content);
                document.getElementById('cool2').appendChild(product);
                sumtot += seshparse.quantity * seshparse.cost_pu;
                console.log(seshparse.quantiy);
            }
        }
        return;
    }

}

function getID(a) {

    var id = a.substring(0, a.length - 4);
    id_quant = document.getElementById(id + '_val').value,
        id_name = document.getElementById(id + '_nme').innerHTML,
        id_cost = document.getElementById(id + '_cst').innerHTML,
        id_cost = id_cost.substring(1, id_cost.length),
        sessionData = sessionStorage.getItem('data'),
        id_delivery = document.getElementById(id + '_con').getAttribute('delivery_unit'),
        JSONData = JSON.parse(sessionStorage.getItem('data'));

    if (id_quant > 0) {
        if (sessionData) { //there is a session

            for (var i = 0; i < JSONData.length; i++) {
                if (JSONData[i].id == id) {
                    var status = i;
                    break;
                } else {
                    var status = null;
                }

            }

            if (status != null) {

                var obj = JSONData;
                obj[status].quantity += parseInt(id_quant);
                sessionStorage.setItem('data', JSON.stringify(obj));
                return;
            } else {

                var obj = {
                    id: id,
                    name: id_name,
                    cost_pu: parseFloat(id_cost),
                    quantity: parseInt(id_quant),
                    order_id: id_delivery
                };
                var getit = JSONData;
                getit.push(obj);
                sessionStorage.setItem('data', JSON.stringify(getit));
            }
        } else { //there is no session
            var ok = [];
            var data = {
                id: id,
                name: id_name,
                cost_pu: parseFloat(id_cost),
                quantity: parseInt(id_quant),
                order_id: id_delivery
            };
            ok.push(data);
            sessionStorage.setItem('data', JSON.stringify(ok));

        }
    }
}

function newBasket() {
    var data = JSON.parse(sessionStorage.getItem('data'));
    for (var i = 0; i < data.length; i++) {
        if (document.getElementById(data[i].id + '_cart') === null) {
            var para = document.createElement("p");
            para.setAttribute("id", data[i].id + "_cart");
            var button = document.createElement("button");
            var txtNode = document.createTextNode('-');
            button.appendChild(txtNode);
            button.setAttribute("id", data[i].id + "_minbut");
            button.addEventListener("click", function() {
                minBut(this.id);
                displayTotal();
            });
            button.setAttribute("class", "sml");
            var content = document.createTextNode(data[i].name + " : " + data[i].quantity + " x £" + data[i].cost_pu)
            para.appendChild(button);
            para.appendChild(content);
            document.getElementById('cool2').appendChild(para);
            // return;
        } else {
            if (data[i].id == 0) {
                document.getElementById(data[i].id + '_cart').innerHTML = '';
            } else {
                document.getElementById(data[i].id + '_cart').innerHTML = "<button id='" + data[i].id + "_minbut' onclick='minBut(this.id); displayTotal();' class='sml'>-</button>" + data[i].name + " : " + data[i].quantity + " x £" + data[i].cost_pu;
            }
        }
    }
}

function displayTotal() {
    var total = 0;
    if (sessionStorage.getItem('data')) {
        var parse = JSON.parse(sessionStorage.getItem('data'));
        for (var i = 0; i < parse.length; i++) {
            if (parse[i].quantity >= 1) {
                total += parse[i].quantity * parse[i].cost_pu;
            }
        }
        document.getElementById('cool3').innerHTML = "Total: £" + checkzero(Math.round(total * 100) / 100);
    }
}

function gotocart() {

    window.location.href = "/test.php";
}
window.onload = show(), displayTotal();