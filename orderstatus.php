<?php
	session_start();
	include 'src/php/connect.php';
	if($_SESSION['logged_in'] != 1){
		header("location: login.php");
	}

	$orderid = stripslashes($_GET['order_id']);
	$userid = $_SESSION['user_id'];

	$stmt = $conn->prepare("SELECT * FROM order_status WHERE user_id = ? AND id = ?");
	$stmt->bind_param("ii", $userid, $orderid);
	$stmt->execute();
	$result = $stmt->get_result();

	$row = $result->fetch_assoc();
	if(mysqli_num_rows($result) == 0){
		header('location: /');
	}
	$contents = json_decode($row['order_contents']);
	$length = sizeof($contents);

	function check($a){
		if(substr($a, -2, 1) == "."){
		$b = $a . "0";
		}else{
		$b = $a;
		}

		return $b;
	}
?>
<html>
<head>
<link rel='stylesheet' href='src/css/index.css' type='text/css'>
<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

</head>
<body>
	<nav>
		<div class="nav-wrapper">
			<div class='container'>
				<a href="/" class="brand-logo sidenav-trigger img_container"><img class='img_logo' src='src/img/gs_logo.png'></a>
				<ul id="mobile-demo" class="right hide-on-med-and-down">
					<li><a href="/about.php">About</a></li>
					<li><a href="/contact.php">Contact</a></li>
					<li><a href="/test.php">Cart <i class="fas fa-shopping-cart fa_src"></i></a></li>
					<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account <i class="fas fa-user fa_src"></i>

	</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<ul id="dropdown1" class="dropdown-content">
		<?php
	 if(isset($_SESSION['logged_in'])){ if($_SESSION['logged_in'] == 1){ echo "
	<li><a href='profile.php'>Profile</a></li>"; echo "
	<li><a href='orders.php'>Orders</a></li>"; echo "
	<li><a href='settings.php'>Settings</a></li>"; echo "
	<li class='divider'></li>"; if($_SESSION['state'] == 1){ echo "
	<li><a href='admin.php'>All Orders</a></li>"; echo "
	<li><a href='order_items.php'>All Products</a></li>"; echo "
	<li class='divider'></li>"; } echo "
	<li><a href='src/php/logout.php'>Logout</a></li>"; } }else{ echo "
	<li><a href='login.php'>Login</a></li>"; echo "
	<li><a href='register.php'>Register</a>
		<li>"; }
	?>
	</ul>
<div class='container' style='margin-top:20px;'>
Thank you for placing an order with us. Below is your order receipt. We have sent this to your email, <?php echo $row['email'] ?>. You can print this by clicking the <b>print</b> button below.<br><br>
</div>
<div class='container' style='border:1px solid black; padding:30px;'>
<div  id='print'>
<div class='row'>
<h4>Environmental Products &amp; Services Ltd</h4>
	<h5>ORDER ID: <?php echo $orderid ?></h5>
	<div class='' id='p_business'><span style='font-weight:600'>Company: </span><?php echo $row['business']; ?></div>
	<div class='' id='p_email'><span style='font-weight:600'>Email: </span><?php echo $row['email']; ?></div>
	<div class='' id='p_phone'><span style='font-weight:600'>Contact no: </span><?php echo $row['phone']; ?></div><br>
	<div class='' id='p_data'><span style='font-weight:600'>Order date: </span><?php echo $row['time_created'] ?></div>
	<br><br>
	<div class='col m12 l6'>
	<h5>Billing Address</h5>
	<div class='' id='p_b1'><?php echo $row['b_addr1'] ?></div>
	<div class='' id='p_b2'><?php echo $row['b_addr2'] ?></div>
	<div class='' id='p_b3'><?php echo $row['b_pc'] ?></div>
	<div class='' id='p_b4'><?php echo $row['b_city'] ?></div>

	</div>
	<div class='col m12 l6'>
	<h5>Delivery Address</h5>
	<div class='' id='p_d1'><?php echo $row['d_addr1'] ?></div>
	<div class='' id='p_d2'><?php echo $row['d_addr2'] ?></div>
	<div class='' id='p_d3'><?php echo $row['d_pc'] ?></div>
	<div class='' id='p_d4'><?php echo $row['d_city'] ?></div>
	</div>
	<div class='col s12'>	<br><br>

	<table>
		<thead>
			<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Quantity</th>
			<th>Individual Cost</th>
			<th>Toal Cost</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$total = 0;
			for($i = 0; $i < $length; $i++){
				echo "<tr>";
				echo "<td>" . $contents[$i]->id . "</td>";
				echo "<td>" . $contents[$i]->name . "</td>";
				echo "<td>" . $contents[$i]->quantity . "</td>";
				echo "<td>£" . $contents[$i]->cost_pu . "</td>";
				echo "<td>£" . check($contents[$i]->cost_total) . "</td>";
				echo "</tr>";

				$total += (float) check($contents[$i]->cost_total);
			}

			?>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td><b>Subtotal: </b></td>
				<td><b>£<?php echo check($total) ?></b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td><b>Delivery: </b></td>
				<td><b>£<?php echo $row['delivery_cost'] . ".00"; ?></b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td><b>Total: </b></td>
				<td><b>£<?php

					echo (floatval($row['delivery_cost']) + floatval(check($total))); ?></b></td>
			</tr>


		</tbody>
	</table>

	</div>

</div>
</div>
</div>
<div id='btn_cont'>
	<button class='btn' onclick='emailme()'>Email</button>
	<button class='btn' onclick='printme()'>Print</button>
	<p>If print fails, please try again.</p>
</div>
<script>
var delay = ( function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

function printme(){
	var contents = document.getElementById('print').innerHTML;
	w = window.open();
	w.document.write("<html><head><link rel='stylesheet' href='src/css/index.css' type='text/css'><link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'><link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'></head><body>" + contents + "</body></html>");
	w.print();

}
</script>
</body>
</html>
