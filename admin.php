<?php
 session_start(); include 'src/php/connect.php'; if($_SESSION['state'] != 1){ header("location: login.php"); }
	function check($a){
		if(substr($a, -2, 1) == "."){
		$b = $a . "0";
		}else{
		$b = $a;
		}

		return $b;
	}
?>
	<html>

	<head>
		<link rel='stylesheet' href='src/css/index.css' type='text/css'>
		<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
		<script src='src/js/redorder.js'></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
		<script>
			$(document).ready(function() {
				$('select').formSelect();
			});

		</script>
	</head>

	<body>
		<nav>
			<div class="nav-wrapper">
				<div class='container'>
					<a href="/" class="brand-logo">EPAS</a>
					<ul id="nav-mobile" class="right hide-on-med-and-down">
						<li><a href="/">About</a></li>
						<li><a href="/">Contact</a></li>
						<li><a href="/test.php">Cart</a></li>
						<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account</a></li>
					</ul>
				</div>
			</div>
		</nav>
		<ul id="dropdown1" class="dropdown-content">
<?php
if(isset($_SESSION['logged_in'])){
	if($_SESSION['logged_in'] == 1){
		echo "<li><a href='profile.php'>Profile</a></li>";
		echo "<li><a href='orders.php'>Orders</a></li>";
		echo "<li><a href='settings.php'>Settings</a></li>";
		echo "<li class='divider'></li>";
		if($_SESSION['state'] == 1){
			echo "<li><a href='admin.php'>All Orders</a></li>";
			echo "<li><a href='order_items.php'>All Products</a></li>";
			echo "<li class='divider'></li>";


		}
		echo "<li><a href='src/php/logout.php'>Logout</a></li>";
	}
}else{
		echo "<li><a href='login.php'>Login</a></li>";
		echo "<li><a href='register.php'>Register</a><li>";
}
?>
		</ul>
		<div class='container' style='margin-top:30px;'>
			<div class='row'>
				<div class='col m12 l2'>
					<input type='search' id='order_search' placeholder='Search by ID'>
				</div>

				<div class='col m12 l10'>

					<?php

	$query = "SELECT * FROM order_status ORDER BY id DESC";

	if($result = $conn->query($query)){

		while($row = $result->fetch_assoc()){




			echo "<div class='id_cont col m12 l6' id='" . $row['id'] . "_cont'>";
			echo "<div class='order_container'>";
			echo "<div class='order_half'>";
			echo "<div class='order_total'>£" . check(floatval($row['total_cost']) + floatval($row['delivery_cost'])) . "</div>";
			echo "<div class='order_id'>" . $row['id'] . "</div>";
			echo "</div>";
			echo "<div class='order_description'>";
			if($row['order_contents']){
				$string = json_decode($row['order_contents']);
				$length = sizeof($string);

				for($i = 0; $i < $length; $i ++){
					echo "<p> ID: " . $string[$i]->id . " Name: " . $string[$i]->name . " x " . $string[$i]->quantity . "</p>";
				}
			}
			echo "</div>";
			echo "<div class='order_bottom'>";
			echo "<div class='order_status'>" . $row['status'] . "</div>";
			echo "<div class='order_date'>" . $row['time_created'] . "</div>";
			echo "</div>";
			echo "<button class='btn' style='margin-top:20px' onclick='checkOrder(" . $row['id'] . ")'>Order Receipt</button><br><br>";
			echo "<form method='post' action='./src/php/adminupdate.php'><label>Status</label>
			  <select class='browser-default' name='status'>
				<option value='' disabled selected>What is the order status?</option>
				<option value='Processing_" . $row['id'] . "'>Processing</option>
				<option value='Failed_" . $row['id'] . "'>Failed</option>
				<option value='Dispatched_" . $row['id'] . "'>Dispatched</option>
				<option value='Arrived_" . $row['id'] . "'>Arrived</option>
				<option value='Unknown_" . $row['id'] . "'>Unknown</option>
			  </select><br>";
			echo "<input type='submit' class='btn'></form>";
			echo "</div></div>";
		}

	}

?>

				</div>
			</div>
		</div>
		<script src='src/js/order.js'></script>
	</body>

	</html>
