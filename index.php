<?php
	session_start();
?>
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>GreaseShield Product Range!</title>
	<link rel='stylesheet' href='src/css/index.css' type='text/css'>
	<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.js"></script>

	<?php	include 'src/php/connect.php';	?>
</head>

<body>

<nav>
	<div class="nav-wrapper">
		<div class='container'>
			<a href="/" class="brand-logo sidenav-trigger img_container"><img class='img_logo' src='src/img/gs_logo.png'></a>
			<ul id="mobile-demo" class="right hide-on-med-and-down">
				<li><a href="/about.php">About</a></li>
				<li><a href="/contact.php">Contact</a></li>
				<li><a href="/test.php">Cart <i class="fas fa-shopping-cart fa_src"></i></a></li>
				<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account <i class="fas fa-user fa_src"></i>

</a></li>
			</ul>
		</div>
	</div>
</nav>
<ul id="dropdown1" class="dropdown-content">
	<?php
 if(isset($_SESSION['logged_in'])){ if($_SESSION['logged_in'] == 1){ echo "
<li><a href='profile.php'>Profile</a></li>"; echo "
<li><a href='orders.php'>Orders</a></li>"; echo "
<li><a href='settings.php'>Settings</a></li>"; echo "
<li class='divider'></li>"; if($_SESSION['state'] == 1){ echo "
<li><a href='admin.php'>All Orders</a></li>"; echo "
<li><a href='order_items.php'>All Products</a></li>"; echo "
<li class='divider'></li>"; } echo "
<li><a href='src/php/logout.php'>Logout</a></li>"; } }else{ echo "
<li><a href='login.php'>Login</a></li>"; echo "
<li><a href='register.php'>Register</a>
	<li>"; }
?>
</ul>
	<div class='container' style='margin-top:20px'>
		<div class='row'>

			<div class='col m12 l2' id='cool'>
				<div class='name'>
					<?php
	if(isset($_SESSION['business']) && $_SESSION['logged_in'] == 1){
		echo "<p id='sessionInfo'><a href='/profile.php'>" . $_SESSION['business'] . "</a></p>";
	}
?>
				</div>
				<div class='input-field'>
					<label for='search_box'>Search...</label>
					<input type='search' id='search_box' onkeyup='search()'>
				</div>

				<h4 id='side'>Your Basket</h4>
				<div id='cool2'>

				</div>
				<div id='cool3'>

				</div>
				<button onclick='gotocart()' class='btn'><i class="cart fas fa-shopping-cart"></i>

</i>

 Cart</button>
			</div>
			<div class='col m12 l10'>
				<?php

	$sql = "SELECT * FROM epas_products";

	$ok = $conn->query($sql);
	while ($row = mysqli_fetch_array($ok, MYSQLI_ASSOC)){
		$type = $row['image_1_type'];
		echo "<div class='col s12 l4 over_container'>";
		echo "<div class='product_container' id='" . $row['uid'] . "_con' delivery_unit='" . $row['delivery_unit'] ."'>";
		echo "<div class='product_image' style='background-image:url(src/img/" . $row['uid'] ."_1." . $type. ")'></div>";
		echo "<div class='product_box'>";
			echo "<div class='product_title' id='" . $row['uid'] . "_nme'>" . $row['name'] . "</div><p class='product_cost' id='". $row['uid'] . "_cst'>£" . $row['cost'] . "</p>";
			echo "<div class='product_description'>" . $row['info'] . "</div>";
			echo "<p class='product_quantity'>Quantity: ";
			echo "<input type='number' class='product_number' min='1' max='20' value='1' id='" . $row['uid'] . "_val'>";
			echo "<button value='ADD TO CART' class='product_add_button' id='" . $row['uid'] . "_btn' onclick='getID(this.id); newBasket(); displayTotal();' >ADD TO CART</button>";
		echo "</p></div></div></div>";

	}
?>

			</div>
		</div>
	</div>

	<footer class="page-footer">

		<div class="footer-copyright">
			<div class="container">
				© 2018 Environmental Products &amp; Services Ltd.

			</div>
		</div>
	</footer>

	<script src='src/js/main.js'>


	</script>
	<script src='src/js/plusmin.js'></script>
</body>

</html>
