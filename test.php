<?php
session_start();
include('src/php/connect.php');
?>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
<link rel='stylesheet' href='src/css/index.css' type='text/css'>
<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

	<script>
    $( document ).ready(function() {

    });
</script>
</head>
<body>
	<nav>
		<div class="nav-wrapper">
			<div class='container'>
				<a href="/" class="brand-logo sidenav-trigger img_container"><img class='img_logo' src='src/img/gs_logo.png'></a>
				<ul id="mobile-demo" class="right hide-on-med-and-down">
					<li><a href="/about.php">About</a></li>
					<li><a href="/contact.php">Contact</a></li>
					<li><a href="/test.php">Cart <i class="fas fa-shopping-cart fa_src"></i></a></li>
					<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account <i class="fas fa-user fa_src"></i>

	</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<ul id="dropdown1" class="dropdown-content">
		<?php
	 if(isset($_SESSION['logged_in'])){ if($_SESSION['logged_in'] == 1){ echo "
	<li><a href='profile.php'>Profile</a></li>"; echo "
	<li><a href='orders.php'>Orders</a></li>"; echo "
	<li><a href='settings.php'>Settings</a></li>"; echo "
	<li class='divider'></li>"; if($_SESSION['state'] == 1){ echo "
	<li><a href='admin.php'>All Orders</a></li>"; echo "
	<li><a href='order_items.php'>All Products</a></li>"; echo "
	<li class='divider'></li>"; } echo "
	<li><a href='src/php/logout.php'>Logout</a></li>"; } }else{ echo "
	<li><a href='login.php'>Login</a></li>"; echo "
	<li><a href='register.php'>Register</a>
		<li>"; }
	?>
	</ul>
<div class='container'  style='margin-top:20px;'>
	<div class='row'>
	<div class='confirm_order col m12 l8' id='order-review'>
	<?php
		$business = '';
		$email = '';
		$phone = '';
		$b1 = '';
		$b2 = '';
		$b3 = '';
		$b4 = '';
		$d1 = '';
		$d2 = '';
		$d3 = '';
		$d4 = '';
		if(isset($_SESSION['business'])){
			echo "Welcome back, " . $_SESSION['business'];
			$business = $_SESSION['business'];
		}
		if(isset($_SESSION['email'])){
			$email = $_SESSION['email'];
		}
		if(isset($_SESSION['phone'])){
			$phone = $_SESSION['phone'];
		}
		if(isset($_SESSION['billing_addr1'])){
			$b1 = $_SESSION['billing_addr1'];
		}

		if(isset($_SESSION['billing_addr2'])){
			$b2 = $_SESSION['billing_addr2'];
		}
		if(isset($_SESSION['billing_pc'])){
			$b3 = $_SESSION['billing_pc'];
		}
		if(isset($_SESSION['billing_city'])){
			$b4 = $_SESSION['billing_city'];
		}
		if(isset($_SESSION['delivery_addr1'])){
			$d1 = $_SESSION['delivery_addr1'];
		}

		if(isset($_SESSION['delivery_addr2'])){
			$d2 = $_SESSION['delivery_addr2'];
		}
		if(isset($_SESSION['delivery_pc'])){
			$d3 = $_SESSION['delivery_pc'];
		}
		if(isset($_SESSION['delivery_city'])){
			$d4 = $_SESSION['delivery_city'];
		}
	?>
	<h3>Order Summary</h3>
	<div id='abc'>

	</div>
	<div id='totalcost'></div>
	<button class='btn' onclick='back()'>Continue Shopping</button>
	<button class='btn' onclick='clearLocal()'>Clear Items</button>
	</div>
	<div class='payment_form col m12 l4'>
		<h5>Please Enter the following information:</h5><br>

		<div class='col s12 m12' id='delivery_address'>
		<h5>Delivery Address:</h5>
		<?php
		if(isset($_SESSION['business'])){
			echo "<input type='text' id='business_name' value='" . $business . "' style='display:none'>";
		}else{
			echo "<div class='input-field'><label for='business_name'>Business</label>";
			echo "<input type='text' id='business_name'></div>";
		}
		if(isset($_SESSION['email'])){
		 	echo "<input type='text' id='email' value='" . $email . "' style='display:none'>";
		}else{
			echo "<div class='input-field'><label for='email'>Email</label>";
			echo "<input type='text' id='email'></div>";
		}
		?>
		<div class='input-field'>
		<label for='phone'>Phone</label>
		<input type='text' id='phone' value='<?php echo $phone;?>'>
		</div>
		<div class='input-field'>
		<label for='d_adrr1'>Address Lines 1</label>
		<input type='text' id='d_addr1' value='<?php echo $d1;?>'>
		</div>
		<div class='input-field'>
		<label for='d_addr2'>Address Line 2</label>
		<input type='text' id='d_addr2' value='<?php echo $d2;?>'>
		</div>
		<div class='input-field'>
		<label for='d_pc'>Postcode</label>
		<input type='text' id='d_pc' value='<?php echo $d3;?>'>
		</div>
		<div class='input-field'>
		<label for='d_city'>City</label>
		<input type='text' id='d_city' value='<?php echo $d4?>'>
		</div>
		<p>
			<input type="checkbox" id="samebill" name='samebill' value='0'/>
			<label for="samebill">Is you Billing Address that same as Delivery?</label>
    	</p>
		<button class='btn' onclick='deliveryAddress()'>Next</button>

		</div>

		<div class='col s12 m12' id='billing_address'>
		<h5>Billing Address:</h5>
		<div class='input-field'>
		<label for='b_addr1'>Address Line 1</label>
		<input type='text' id='b_addr1' value='<?php echo $b1;?>'>
		</div>
		<div class='input-field'>
		<label for='b_addr2'>Address Line 2</label>
		<input type='text' id='b_addr2' value='<?php echo $b2;?>'>
		</div>
		<div class='input-field'>
		<label for='b_pc'>Postcode</label>
		<input type='text' id='b_pc' value='<?php echo $b3;?>'>
		</div>
		<div class='input-field'>
		<label for='b_city'>City</label>
		<input type='text' id='b_city' value='<?php echo $b4;?>'>
		</div>
		<button class='btn' onclick='goBack()'>Back</button>
		<button class='btn' onclick='billingAddress()'>Next</button>

		</div>

		<div class='col s12 m12' id='card_details'>
		<input type='checkbox' id='d_city' placeholder='City'>
		<h5>Card Information:</h5>
		<div class='input-field'>
		<label for='f_name'>First Name</label>
		<input type='text' id='f_name'>
		</div>
		<div class='input-field'>
		<label for='l_name'>Last Name</label>
		<input type='text' id='l_name'>
		</div>
		<div class='input-field'>
		<label for='card_number'>Card Number</label>
		<input type='text' id='card_number'>
		</div>
		<div class='input-field'>
		<label for='exp'>Expiry MM/YY</label>
		<input type='text' id='exp'>
		</div>
		<div class='input-field'>
		<label for='cvc'>CVC</label>
		<input type='number' id='cvc'>
		</div>
		<button class='btn' onclick='goBack()'>Back</button>
		<button class='btn' onclick='post()'>POST</button>
		</div>
		<br>
		<div id='responseid' class='error-response'></div>
			<img src='src/img/visa-mastercard.png' id='visa-img'>
	</div>
	</div>
</div>
<footer class="page-footer">

  <div class="footer-copyright">
	<div class="container">
	© 2018 Environmental Products &amp; Services Ltd.

	</div>
  </div>
</footer>
<script src='src/js/post.js'></script>
</body>
</html>
