<?php
	session_start();
	include 'src/php/connect.php';

	if($_SESSION['logged_in'] != 1){
		header("location: login.php");
	}

?>
<html>
<head>
<link rel='stylesheet' href='src/css/index.css' type='text/css'>
<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
</head>
<body>
	<nav>
		<div class="nav-wrapper">
			<div class='container'>
				<a href="/" class="brand-logo sidenav-trigger img_container"><img class='img_logo' src='src/img/gs_logo.png'></a>
				<ul id="mobile-demo" class="right hide-on-med-and-down">
					<li><a href="/about.php">About</a></li>
					<li><a href="/contact.php">Contact</a></li>
					<li><a href="/test.php">Cart <i class="fas fa-shopping-cart fa_src"></i></a></li>
					<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account <i class="fas fa-user fa_src"></i>

	</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<ul id="dropdown1" class="dropdown-content">
		<?php
	 if(isset($_SESSION['logged_in'])){ if($_SESSION['logged_in'] == 1){ echo "
	<li><a href='profile.php'>Profile</a></li>"; echo "
	<li><a href='orders.php'>Orders</a></li>"; echo "
	<li><a href='settings.php'>Settings</a></li>"; echo "
	<li class='divider'></li>"; if($_SESSION['state'] == 1){ echo "
	<li><a href='admin.php'>All Orders</a></li>"; echo "
	<li><a href='order_items.php'>All Products</a></li>"; echo "
	<li class='divider'></li>"; } echo "
	<li><a href='src/php/logout.php'>Logout</a></li>"; } }else{ echo "
	<li><a href='login.php'>Login</a></li>"; echo "
	<li><a href='register.php'>Register</a>
		<li>"; }
	?>
	</ul>
	<div class='container' style='margin-top:30px'>
	<div class='row'>
	<div class='col m12 l2'></div>
	<div class='col m12 l10'>
		<h4>Information</h4>
		<?php

			$sql = "SELECT * FROM users WHERE id = " . $_SESSION['user_id'];
			$conn->query($sql);
			$business = '';
			$phone = '';
			$d1 = '';
			$d2 = '';
			$d3 = '';
			$d4 = '';
			$b1 = '';
			$b2 = '';
			$b3 = '';
			$b4 = '';
			if($result = $conn->query($sql)){
				while($res = $result->fetch_assoc()){
					if(isset($res['business'])){
						$business = $res['business'];
					}
					if(isset($res['phone'])){
						$phone = $res['phone'];
					}
					if(isset($res['d_addr1'])){
						$d1 = $res['d_addr1'];
					}
					if(isset($res['d_addr2'])){
						$d2 = $res['d_addr2'];
					}
					if(isset($res['d_pc'])){
						$d3 = $res['d_pc'];
					}
					if(isset($res['d_city'])){
						$d4 = $res['d_city'];
					}
					if(isset($res['b_addr1'])){
						$b1 = $res['b_addr1'];
					}
					if(isset($res['b_addr2'])){
						$b2 = $res['b_addr2'];
					}
					if(isset($res['b_pc'])){
						$b3 = $res['b_pc'];
					}
					if(isset($res['b_city'])){
						$b4 = $res['b_city'];
					}
				}
			}
		?>
		<form method='post'>
		<div class='col m12 l6'>
			<label for='business'>Business</label>
			<input type='text' id='business' value='<?php echo $business; ?>' name='business'>
		</div>
		<div class='col m12 l6'>
			<label for='phone'>Phone no</label>
			<input type='text' id='phone' value='<?php echo $phone; ?>' name='phone'>
		</div>
		<h4>Delivery Address</h4><br>
		<div class='col m12 l6'>
			<label for='d_addr1'>Address Line 1</label>
			<input type='text' id='d_addr1' value='<?php echo $d1; ?>' name='d1'>
		</div>
		<div class='col m12 l6'>
			<label for='d_addr2'>Address Line 2</label>
			<input type='text' id='d_addr2' value='<?php echo $d2; ?>' name='d2'>
		</div>
		<div class='col m12 l6'>
			<label for='d_pc'>Post Code</label>
			<input type='text' id='d_pc' value='<?php echo $d3; ?>' name='d3'>
		</div>
		<div class='col m12 l6'>
			<label for='d_city'>City</label>
			<input type='text' id='d_city' value='<?php echo $d4; ?>' name='d4'>
		</div>
		<h4>Billing Address</h4><br>
		<div class='col m12 l6'>
			<label for='b_addr1'>Address Line 1</label>
			<input type='text' id='b_addr1' value='<?php echo $b1; ?>' name='b1'>
		</div>
		<div class='col m12 l6'>
			<label for='b_addr2'>Address Line 2</label>
			<input type='text' id='b_addr2' value='<?php echo $b2; ?>' name='b2'>
		</div>
		<div class='col m12 l6'>
			<label for='b_pc'>Post Code</label>
			<input type='text' id='b_pc' value='<?php echo $b3; ?>' name='b3'>
		</div>
		<div class='col m12 l6'>
			<label for='b_city'>City</label>
			<input type='text' id='b_city' value='<?php echo $b4; ?>' name='b4'>
		</div>
		<div class='col m12 l6'>
		<input type='submit' class='btn' value='SUBMIT'>
		<?php
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				$p1 = stripslashes($_POST['business']);
				$p2 = stripslashes($_POST['phone']);
				$p3 = stripslashes($_POST['b1']);
				$p4 = stripslashes($_POST['b2']);
				$p5 = stripslashes($_POST['b3']);
				$p6 = stripslashes($_POST['b4']);
				$p7 = stripslashes($_POST['d1']);
				$p8 = stripslashes($_POST['d2']);
				$p9 = stripslashes($_POST['d3']);
				$p10 = stripslashes($_POST['d4']);

				$stmt = $conn->prepare("UPDATE users SET business = ?, phone = ?, b_addr1 = ?, b_addr2 = ?, b_pc = ?, b_city = ?, d_addr1 = ?, d_addr2 = ?, d_pc = ?, d_city = ? WHERE id = ?");
				$stmt->bind_param("sssssssssss", $p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$_SESSION['user_id']);
				$stmt->execute();

				$_SESSION['business'] = $p1;
				$_SESSION['phone'] = $p2;
				$_SESSION['billing_addr1'] = $p3;
				$_SESSION['billing_addr2'] = $p4;
				$_SESSION['billing_pc'] = $p5;
				$_SESSION['billing_city'] = $p6;
				$_SESSION['delivery_addr1'] = $p7;
				$_SESSION['delivery_addr2'] = $p8;
				$_SESSION['delivery_pc'] = $p9;
				$_SESSION['delivery_city'] = $p10;

				header("location: profile.php");

			}
		?>
		</div>
		</form>
	</div>
	</div>
	</div>
</body>
</html>
