function checkzero(a) {

    a = "" + a;
    if (a.indexOf(".") != -1) {
        var ok = a.substring(a.length - 2, a.length - 1);
        if (ok == ".") {
            return parseFloat(a) + "0";
        } else {
            return parseFloat(a);
        }
    } else {
        return parseFloat(a);
    }

}

function dcc() {

    var objStorage = JSON.parse(sessionStorage.getItem('data'));
    var total = 0;
    for (var i = 0; i < objStorage.length; i++) {
        total += objStorage[i].quantity * Number(objStorage[i].order_id);
    }

    return (15 * Math.ceil(total / 15));
}

function back() {
    window.location.href = "/index.php";
}

function clearLocal() {
    sessionStorage.clear();
    window.location.href = "/index.php";
}

function clearAll(order_id) {

    var mucho = sessionStorage.getItem('data');
    sessionStorage.setItem('need', mucho);
    var elements = document.getElementsByTagName("input");
    for (var i = 0; i < elements.length; i++) {
        elements[i].value = "";
    };
    window.setTimeout(function() {
        location.href = '/orderstatus.php?order_id=' + order_id;
        sessionStorage.removeItem('data');
    }, 2000)

}

function review() {

    var data = JSON.parse(sessionStorage.getItem('data')),
        sumtotal = 0;
    if (data) {
        for (var i = 0; i < data.length; i++) {


            if (data[i].quantity >= 1) {
                var dataObj = JSON.parse(sessionStorage.getItem('data'))[i]

                var uid = dataObj.id;

                sumtotal += parseInt(dataObj.quantity) * parseFloat(dataObj.cost_pu);

                var product_elem = document.createElement("p");
                product_elem.setAttribute("id", uid + "_fc");
                product_elem.setAttribute("class", "final_cost");
                var content = document.createTextNode(dataObj.quantity + " x " + dataObj.name + " = £" + checkzero(Math.round(parseInt(dataObj.quantity) * parseFloat(dataObj.cost_pu) * 100) / 100));
                product_elem.appendChild(content);
                document.getElementById('abc').appendChild(product_elem);
            }
        }
    }

    document.getElementById('totalcost').innerHTML = "SubTotal: £" + (Math.round(sumtotal * 100) / 100).toFixed(2) + "<br> Delivery: £" + dcc() + "<br> Total: £" + (Number(dcc()) + Math.round(sumtotal * 100) / 100).toFixed(2);
    document.getElementById('totalcost').style.fontWeight = 600;

}

review();

function goBack() {
    if (document.getElementById("delivery_address").style.display == "block") {
        document.getElementById('billing_address').style.display = 'none';
        document.getElementById('delivery_address').style.display = 'block';
        document.getElementById('card_details').style.display = 'none';
    }
    if (document.getElementById("billing_address").style.display == "block") {
        document.getElementById('billing_address').style.display = 'none';
        document.getElementById('delivery_address').style.display = 'block';
        document.getElementById('card_details').style.display = 'none';
    }
    if (document.getElementById("card_details").style.display == "block") {
        document.getElementById('billing_address').style.display = 'block';
        document.getElementById('delivery_address').style.display = 'none';
        document.getElementById('card_details').style.display = 'none';
    }
}

function toCard() {
    document.getElementById('billing_address').style.display = 'none';
    document.getElementById('delivery_address').style.display = 'none';
    document.getElementById('card_details').style.display = 'block';
}

function toBill() {
    document.getElementById('billing_address').style.display = 'block';
    document.getElementById('delivery_address').style.display = 'none';
    document.getElementById('card_details').style.display = 'none';
}

function deliveryAddress() {

    var d_addr1 = document.getElementById('d_addr1').value,
        d_addr2 = document.getElementById('d_addr2').value,
        d_pc = document.getElementById('d_pc').value,
        d_city = document.getElementById('d_city').value,
        same = document.getElementById('samebill').checked;

    if (d_addr1 && d_addr2 && d_pc && d_city) {

        if (same == true) {

            document.getElementById('b_addr1').value = d_addr1;
            document.getElementById('b_addr2').value = d_addr2;
            document.getElementById('d_pc').value = d_pc;
            document.getElementById('d_city').value = d_city;

            toCard();
        } else {

            toBill();
        }

    } else {
        document.getElementById('responseid').innerHTML = "Please fill in the fields";
        return;
    }


}

function billingAddress() {

    var b_addr1 = document.getElementById('b_addr1').value,
        b_addr2 = document.getElementById('b_addr2').value,
        b_pc = document.getElementById('b_pc').value,
        b_city = document.getElementById('b_city').value;

    if (b_addr1 && b_addr2 && b_pc && b_city) {

        toCard();

    } else {
        document.getElementById('responseid').innerHTML = "Please fill in the fields";
    }
}





function post() { //send info

    var card_f_name = document.getElementById('f_name').value,
        card_l_name = document.getElementById('l_name').value,
        name = card_f_name + " " + card_l_name,
        card_number = document.getElementById('card_number').value,
        card_exp = document.getElementById('exp').value,
        card_exp_month = card_exp.split("/")[0],
        card_exp_year = "20" + card_exp.split("/")[1],
        card_cvc = document.getElementById('cvc').value,
        sumtotal = 0,
        data = sessionStorage.getItem('data'),
        description = '',
        status = 0, //document.getElementById('status').value,
        d_addr1 = document.getElementById('d_addr1').value,
        d_addr2 = document.getElementById('d_addr2').value,
        d_pc = document.getElementById('d_pc').value,
        d_city = document.getElementById('d_city').value,
        b_addr1 = document.getElementById('b_addr1').value,
        b_addr2 = document.getElementById('b_addr2').value,
        b_pc = document.getElementById('b_pc').value,
        b_city = document.getElementById('b_city').value,
        business = document.getElementById('business_name').value,
        email = document.getElementById('email').value,
        phone = document.getElementById('phone').value;


    if (JSON.parse(data) == null) {
        document.getElementById('responseid').innerHTML = "Nothing in cart";
        return;
    }
    //card_cvc.toString().length == 3 && card_number.toString().length == 16
    if (card_cvc.toString().length == 3 && card_number.toString().length == 16) {

        var sendarray = [];

        for (var i = 0; i < JSON.parse(data).length; i++) {

            var dataObj = JSON.parse(sessionStorage['data'])[i];

            sendarray.push({
                id: dataObj.id,
                quantity: dataObj.quantity
            });


        }

        var request = new XMLHttpRequest();
        var method = 'post';
        var url = 'http://localhost:8080/postdata';

        var content = {
            "order": sendarray,
            "first_name": card_f_name,
            "last_name": card_l_name,
            "business": business,
            "email": email,
            "phone": phone,
            "deliveryAddress": {
                "address1": d_addr1,
                "address2": d_addr2,
                "postalCode": d_pc,
                "city": d_city
            },
            "billingAddress": {
                "address1": b_addr1,
                "address2": b_addr2,
                "postalCode": b_pc,
                "city": b_city
            },
            "payment": {
                "name": name,
                "expiryMonth": card_exp_month,
                "expiryYear": card_exp_year,
                "cardNumber": card_number,
                "cvc": card_cvc
            }


        };
        request.open(method, url);

        request.setRequestHeader("Content-Type", "application/json");

        request.send(JSON.stringify(content));


        request.onreadystatechange = function() {

            var paymentState = request.responseText;
            console.log(request.responseText);

            var order_id = JSON.parse(paymentState).orderid;

            switch (JSON.parse(paymentState).payment) {
                case "ACCEPTED":
                    document.getElementById('responseid').innerHTML = "Payment Succesful, you have been charged: £" + JSON.parse(paymentState).amount + "\r\n We are redirecting you to a summary of your order. You are order: " + JSON.parse(paymentState).orderid;
                    document.getElementById('responseid').style.color = "green";
                    clearAll(order_id);
                    break;
                case "FAILED":
                    document.getElementById('responseid').innerHTML = "Payment has failed, please try again";
                    break;
                default:
                    document.getElementById('responseid').innerHTML = "There has been an error. Please refresh the page and try again. If this errors persists please call: 028 3083 3081";

            }
        }
    } else {
        document.getElementById('responseid').innerHTML = "Please ensure you have entered the correct card information";
        return;
    }


}