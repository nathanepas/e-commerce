function plus1(a) {

	var id = a.substring(0, a.length - 4);
	var obj = JSON.parse(sessionStorage.getItem('data'));
	obj[id].quantity++;
	sessionStorage.setItem('data', JSON.stringify(obj));

	show();
}

function minus1(a) {

	var id = a.substring(0, a.length - 4);
	var obj = JSON.parse(sessionStorage.getItem('data'));
	obj[id].quantity--;
	sessionStorage.setItem('data', JSON.stringify(obj));

	show();

}
