<?php
/*

Verifies an email address, if no verification, one cannot login.

Check that email address exists with the verify code. If results length == 1, then we update verified to 1 (from 0)
If results length != 1, nothing happens.

*/
include 'src/php/connect.php';

$id = stripslashes($_GET['id']);
$ver = stripslashes($_GET['ver']);

$stmt = $conn->prepare("SELECT * FROM users WHERE id = ? AND email_verify_code = ?");
$stmt->bind_param("is", $id, $ver);
$stmt->execute();
$result = $stmt->get_result();
$resultrows = mysqli_num_rows($result);
if($resultrows == 1){
    $stmt2 = $conn->prepare("UPDATE users SET email_verify = 1 WHERE id = ?");
    $stmt2->bind_param("i", $id);
    $stmt2->execute();
    header("location: login.php");
}else{
    echo "error";
}
?>
