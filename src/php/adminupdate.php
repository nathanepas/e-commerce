<?php
session_start();
include("connect.php");

function changeID($a){
		return explode("_", $a);
}

$input = $_POST["status"];

$state = changeID($input)[0];
$id = changeID($input)[1];

$stmt = $conn->prepare("UPDATE order_status SET status = ? WHERE id = ?");
$stmt->bind_param("si", $state, $id);
$stmt->execute();

header('location: ../../admin.php');
?>