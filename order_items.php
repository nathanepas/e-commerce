<?php
	session_start();
	include 'src/php/connect.php';
	if($_SESSION['state'] != 1){
		header("location: login.php");
	}

	$stmt = $conn->prepare("SELECT * FROM epas_products");
	$stmt->execute();

	$result = $stmt->get_result();
?>
	<html>

	<head>
		<link rel='stylesheet' href='src/css/index.css' type='text/css'>
		<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.js"></script>
	</head>

	<body>
		<nav>
			<div class="nav-wrapper">
				<div class='container'>
					<a href="/" class="brand-logo sidenav-trigger img_container"><img class='img_logo' src='src/img/gs_logo.png'></a>
					<ul id="mobile-demo" class="right hide-on-med-and-down">
						<li><a href="/about.php">About</a></li>
						<li><a href="/contact.php">Contact</a></li>
						<li><a href="/test.php">Cart <i class="fas fa-shopping-cart fa_src"></i></a></li>
						<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account <i class="fas fa-user fa_src"></i>

		</a></li>
					</ul>
				</div>
			</div>
		</nav>
		<ul id="dropdown1" class="dropdown-content">
			<?php
		 if(isset($_SESSION['logged_in'])){ if($_SESSION['logged_in'] == 1){ echo "
		<li><a href='profile.php'>Profile</a></li>"; echo "
		<li><a href='orders.php'>Orders</a></li>"; echo "
		<li><a href='settings.php'>Settings</a></li>"; echo "
		<li class='divider'></li>"; if($_SESSION['state'] == 1){ echo "
		<li><a href='admin.php'>All Orders</a></li>"; echo "
		<li><a href='order_items.php'>All Products</a></li>"; echo "
		<li class='divider'></li>"; } echo "
		<li><a href='src/php/logout.php'>Logout</a></li>"; } }else{ echo "
		<li><a href='login.php'>Login</a></li>"; echo "
		<li><a href='register.php'>Register</a>
			<li>"; }
		?>
		</ul>

		<div class='container'>
			<div class='row'>
				<?php
				echo "
	<form method='post' action='./src/php/updateproduct.php' enctype='multipart/form-data'>
	<div class='a_outer col s12 l6'>
		<div class='a_container'>
		<div class='a_id col s12 l6'>
			New Part
		</div>
		<div class='input-field col s12 l6'>
			<label for='a_uid'>Part Number</label>
			<input type='text' id='a_uid' name='id' value=''>
		</div>

		<div class='input-field col s12 l6'>
			<label for='a_name'>Name</label>
			<input type='text' id='a_name' name='name' value=''>
		</div>

		<div class='input-field col s12 l6'>
			<label for='a_info'>Info</label>
			<input type='text' id='a_info' name='info' value=''>
		</div>

		<div class='input-field col s12 l6'>
			<label for='a_cost'>Cost</label>
			<input type='text' id='a_cost' name='cost' value=''>
		</div>

		<div class='input-field col s12 l6'>
			<label for='a_du'>Delivery Unit</label>
			<input type='text' id='a_du' name='du' value=''>
		</div>

		<input type='file' name='file'><br>

		<input type='hidden' name='hidden_box' value='new'>
		<input type='submit' value='ADD' class='btn a_btn green'>
		</div>
	</div>
	</form>";
	while($row = $result->fetch_array()){
echo "
	<div class='a_outer col s12 l6'>
		<div class='a_container'>
		<form method='post' action='./src/php/updateproduct.php' enctype='multipart/form-data'>
		<div class='a_id col s12 l6'>
			" . $row['uid'] . "
		</div>
		<div class='input-field col s12 l6'>
			<label for='a_uid'>Part Number</label>
			<input type='text' id='a_uid' name='id' value='" . $row['id'] ."'>
		</div>

		<div class='input-field col s12 l6'>
			<label for='a_name'>Name</label>
			<input type='text' id='a_name' name='name' value='" . $row['name'] ."'>
		</div>

		<div class='input-field col s12 l6'>
			<label for='a_info'>Info</label>
			<input type='text' id='a_info' name='info' value='" . $row['info'] ."'>
		</div>

		<div class='input-field col s12 l6'>
			<label for='a_cost'>Cost</label>
			<input type='text' id='a_cost' name='cost' value='" . $row['cost'] ."'>
		</div>

		<div class='input-field col s12 l6'>
			<label for='a_du'>Delivery Unit</label>
			<input type='text' id='a_du' name='du' value='" . $row['delivery_unit'] ."'>
		</div>
		<div class='col s12 l6'>
		<input type='hidden' name='hidden_box' value='" . $row['uid'] ."'>
		<input type='submit' value='Update' class='btn a_btn'>
		</div>
		</form>
		<form method='post' action='./src/php/delete_item.php'>
		<input type='hidden' name='delete_box' value='" . $row['uid'] . "'>
		<input type='submit' value='DELETE' class='btn a_del'>
		</form>
		</div>
	</div>
	";

	}
	?>

			</div>
		</div>
	</body>

	</html>
