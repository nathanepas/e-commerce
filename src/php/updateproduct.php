<?php
session_start();
include("connect.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	$uid = $_POST['hidden_box'];
	$id = $_POST['id'];
	$name = $_POST['name'];
	$info = $_POST['info'];
	$cost = $_POST['cost'];
	$du = $_POST['du'];


	if($id != null){
		if($uid == "new"){

			$file = $_FILES['file'];
			$filename = $_FILES['file']['name'];
			$fileTemp = $_FILES['file']['tmp_name'];
			$fileType = $_FILES['file']['type'];
			$fileType = explode("/", $fileType);

			if($fileType[1] == "png" | $fileType[1] == "gif" | $fileType[1] == "jpeg" | $fileType[1] == "jpg"){
				$stmt = $conn->prepare("INSERT INTO epas_products (id, name, info, cost, delivery_unit, image_1_type) VALUES  (?, ?, ?, ?, ?, ?)");
				$stmt->bind_param("ssssss", $id, $name, $info, $cost, $du, $fileType[1]);
				$stmt->execute();

				$stmt2 = $conn->prepare("SELECT uid FROM epas_products WHERE id = ?");
				$stmt2->bind_param("s", $id);
				$stmt2->execute();
				$result = $stmt2->get_result();
				$resultid;
				$i = 0;
				while($row = $result->fetch_array()){
					$resultid = $row['uid'];
					$i++;
				}
					$filepath = "./../img/"  . $resultid . "_1." .$fileType[1];
					move_uploaded_file($fileTemp, $filepath);
			}else{
				echo "filetype not supported";
			}
		}else{

		$stmt = $conn->prepare("UPDATE epas_products SET id = ?, name = ?, info = ?, cost = ?, delivery_unit = ? WHERE uid = ?");
		$stmt->bind_param("ssssss", $id, $name, $info, $cost, $du, $uid);
		$stmt->execute();
		}
	}
	header('location: ../../order_items.php');
}
?>
