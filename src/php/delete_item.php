<?php
session_start();
include("connect.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	$id = $_POST['delete_box'];
	$stmt = $conn->prepare("DELETE FROM epas_products WHERE uid = ?");
	$stmt->bind_param("i" , $id);
	$stmt->execute();
	header("location: ./../../order_items.php");
}
?>
