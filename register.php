<?php
	session_start();
	include 'src/php/connect.php';

	if(isset($_SESSION['logged_in'])){
		header("location: index.php");
	}

	function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}
?>
<html>
<head>
<link rel='stylesheet' href='src/css/index.css' type='text/css'>
<link rel='stylesheet' href='src/css/materialize.min.css' type='text/css'>
<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<script>
    $( document ).ready(function() {
		$('.modal').modal();

    });
</script>
</head>
<body>
	<nav>
		<div class="nav-wrapper">
			<div class='container'>
				<a href="/" class="brand-logo sidenav-trigger img_container"><img class='img_logo' src='src/img/gs_logo.png'></a>
				<ul id="mobile-demo" class="right hide-on-med-and-down">
					<li><a href="/about.php">About</a></li>
					<li><a href="/contact.php">Contact</a></li>
					<li><a href="/test.php">Cart <i class="fas fa-shopping-cart fa_src"></i></a></li>
					<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Account <i class="fas fa-user fa_src"></i>

	</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<ul id="dropdown1" class="dropdown-content">
		<?php
	 if(isset($_SESSION['logged_in'])){ if($_SESSION['logged_in'] == 1){ echo "
	<li><a href='profile.php'>Profile</a></li>"; echo "
	<li><a href='orders.php'>Orders</a></li>"; echo "
	<li><a href='settings.php'>Settings</a></li>"; echo "
	<li class='divider'></li>"; if($_SESSION['state'] == 1){ echo "
	<li><a href='admin.php'>All Orders</a></li>"; echo "
	<li><a href='order_items.php'>All Products</a></li>"; echo "
	<li class='divider'></li>"; } echo "
	<li><a href='src/php/logout.php'>Logout</a></li>"; } }else{ echo "
	<li><a href='login.php'>Login</a></li>"; echo "
	<li><a href='register.php'>Register</a>
		<li>"; }
	?>
	</ul>
<div class='container'>
<div class='row'>
	<form method='post' action=''>
	<div class='col s12 m8' id='login_form'>
		Register
		<div class='input-field'>
			<label for='email'>Email</label>
			<input type='email' id='email' name='email'>
		</div>
		<div class='input-field'>
			<label for='business'>Business</label>
			<input type='text' id='business' name='business'>
		</div>
		<div class='input-field'>
			<label for='contact_no'>Contact no</label>
			<input type='text' id='contact_no' name='contact_no'>
		</div>
		<div class='input-field'>
			<label for='password'>Password</label>
			<input type='password' id='password' name='password'>
		</div>
		<div class='input-field'>
			<label for='r_password'>Repeat Password</label>
			<input type='password' id='r_password' name='r_password'>
		</div>
		<p>
			<input type="checkbox" id="terms" name='terms'/>
			<label for="terms"></label>
    	</p>Please ensure you have read our <a href='#'>Terms &amp; Conditions</a> as well as our <a href="#modal1" class='modal-trigger'>Data Handling Policy</a>
		<br><br>
		<input type='submit' class='btn'>
			<?php
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				if($_POST['email'] != null && $_POST['password'] != null && $_POST['r_password'] != null && $_POST['business'] != null && $_POST['contact_no'] != null && !empty($_POST['terms'])){
					if($_POST['password'] == $_POST['r_password']){

						$email = stripslashes($_POST['email']);
						$password = stripslashes($_POST['password']);
						$business = stripslashes($_POST['business']);
						$contact = stripslashes($_POST['contact_no']);
						$state = $_POST['terms'];
						$stmt = $conn->prepare("SELECT id FROM users WHERE email = ?");
						$stmt->bind_param("s",$email);
						$stmt->execute();
						$result = $stmt->get_result();
						$resultrows = mysqli_num_rows($result);

						if($resultrows == 0){

							$hashedpass = password_hash($password, PASSWORD_DEFAULT);
							$randomcode = random_str(64);
							$pass_reset = random_str(64);
							$stmt2 = $conn->prepare("INSERT INTO users(email,password_hash,business,phone,email_verify_code,password_reset) VALUES (?,?,?,?,?,?)");
							$stmt2->bind_param("ssssss", $email, $hashedpass, $business, $contact, $randomcode, $pass_reset);
							$stmt2->execute();
							/*
							$msg = "localhost/verify?a=" . $randomcode . "?b=" . $email;
							$headers = "From: nathan@localhost.com";
							mail($email,"Verify Password",$msg,$headers);
							*/
							echo "<p style='color:green'>You have successfully register an account with us for the following email: " . $email . ". Please check your emails to verify your account.</p>";
						}else{
							echo "<p style='color:red'>Email already in use</p>";
						}

					}else{
						echo "<p style='color:red'>Passwords do not match</p>";
					}
				}else{
					echo "<p style='color:red'>Please fill in any missing form</p>";
				}
			}
			?>
	</div>

	</form>
	<div id="modal1" class="modal">
      <div class="modal-content">
        <h4>Date Handling Policy</h4>
		<ol>
			<li>
				Intended use of Personal Data:<br />
				To purchase an item from EPAS, we require
				<ol>
					<li>

					</li>
				</ol>
			</li>
		</ol>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
      </div>
    </div>

</div>
</div>
</body>
</html>
