<?php

	$dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "plc";

    // Create connection
    $conn = new mysqli($dbhost,$dbuser,$dbpass,$dbname);

	if($conn->connect_error){
		header("location: noconnection.php");
	}

	return $conn;


?>
